import React, { useEffect, useState } from 'react';
import './App.css';
import Tmdb from './Tmdb';
import MovieRow from './components/MovieRow.js';
import FeatureMovie from './components/FeatureMovie';
import Header from './components/Header/Header.js';

export default () => {

  const [movieList, setMovieList] = useState([]);
  const [featureData, setFeatureData] = useState([]);
  const [blackHeader, setBlackHeader] = useState(false);

  useEffect(() => {
    const loadAll = async () => {
      let list = await Tmdb.getHomeList(); // Pegando lista geral
      setMovieList(list);

      let originals = list.filter(e => e.slug === "originals"); // Pegando o feature
      let randomChosen = Math.floor(Math.random() * (originals[0].items.results.length - 1));
      let chosen = originals[0].items.results[randomChosen];
      let chosenInfo = await Tmdb.getMovieInfo(chosen.id, 'tv');
      setFeatureData(chosenInfo);
      console.log(featureData, chosenInfo)
    }

    loadAll();
  }, []);

  useEffect(() => {
    const scrollListener = () => {
      if (window.scrollY > 10) {
        setBlackHeader(true);
      } else {
        setBlackHeader(false);
      }
    }

    window.addEventListener('scroll', scrollListener);

    return () => {
      window.removeEventListener('scroll', scrollListener);
    }
  })

  return(
    <div className="page">

      <Header blackHeader={blackHeader}/>

      {featureData && <FeatureMovie item={featureData} />}

      <section className="lists">
        {movieList.map((elem, index) => {
          return (
            <MovieRow key={index}
              title={elem.title}
              items={elem.items}
            />
          )
        })}
      </section>

      <footer>
        Desenvolvido por Jackson de Almeida<br/>
        Direitos de imagem para Netflix<br/>
        Dados retirados do site Themoviedb.org
      </footer>
      
      {movieList.length <= 0 &&
        <div className="loading">
          <img src="https://media.wired.com/photos/592744d3f3e2356fd800bf00/master/w_2560%2Cc_limit/Netflix_LoadTime.gif" alt="loading netflix clone"/>
        </div>
      }
    </div>
  );
}
